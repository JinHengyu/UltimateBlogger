// require 过的文件会加载到缓存，所以多次 require 同一个文件（模块）不会重复加载

const path = require('path')
const express = require('express')
const session = require('express-session')
const MongoStore = require('connect-mongo')(session)
const flash = require('connect-flash')
// json必须用双引号..
global["config"] = require('./config/config.json')
const pkg = require('./package') // package.json
const routes = require('./routes/index')
const winston = require('winston')
const expressWinston = require('express-winston')

// 全局的各种文件和目录的路径,供所有模块共享
global['all_path'] = {
  about: __dirname + '/views/about.html',
  readme: __dirname + '/README.md',
  static: path.join(__dirname, 'public'),
  view: path.join(__dirname, 'views')
}


// 通过express方法生成新的app对象
const app = express()

// 设置模板寻找目录
app.set('views', global.all_path.view)
console.log(app.get('views'))
// 设置模板引擎为 ejs
app.set('view engine', 'ejs')



/* 开始路由 */


// 静态文件:默认目录里的所有文件挂载在根'/'下
// 无论是javaee还是nodejs,只有静态文件才需要考虑url路径,动态网页以及各种脚本都得通过映射
app.use(express.static(global.all_path.static))
// session 中间件
app.use(session({
  name: config.session.key, // 设置 cookie 中保存 session id 的字段名称
  secret: config.session.secret, // 通过设置 secret 来计算 hash 值并放在 cookie 中，使产生的 signedCookie 防篡改
  resave: true, // 强制更新 session
  saveUninitialized: false, // 设置为 false，强制创建一个 session，即使用户未登录
  cookie: {
    maxAge: config.session.maxAge // 过期时间，过期后 cookie 中的 session id 自动删除
  },
  store: new MongoStore({ // 将 session 存储到 mongodb
    url: config.mongodb // mongodb 地址
  })
}))
// flash 中间件，用来显示通知
app.use(flash())

// 处理表单及文件上传的中间件
app.use(require('express-formidable')({
  uploadDir: global.all_path.static + '/img', // 上传文件目录
  keepExtensions: true // 保留后缀
}))

// 设置模板全局常量(application level)
// 相当于servlet中的setAttribute()..
app.locals.blog = {
  title: pkg.name,
  description: pkg.description
}

// 添加模板必需的三个变量
app.use(function (req, res, next) {
  res.locals.user = req.session.user
  res.locals.success = req.flash('success').toString()
  res.locals.error = req.flash('error').toString()
  next()
})

// 正常请求的日志
app.use(expressWinston.logger({
  transports: [
    new(winston.transports.Console)({
      json: true,
      colorize: true
    }),
    new winston.transports.File({
      filename: 'logs/success.log'
    })
  ]
}))
// 路由相当于servlet中的重分发(跳转)
routes(app)
// 错误请求的日志
app.use(expressWinston.errorLogger({
  transports: [
    new winston.transports.Console({
      json: true,
      colorize: true
    }),
    new winston.transports.File({
      filename: 'logs/error.log'
    })
  ]
}))
// 到这一步一定出现error了
app.use(function (err, req, res, next) {
  console.error(err)
  req.flash('error', err.message)
  res.redirect('/posts')
})

if (module.parent) {
  // 被 require，则导出 app
  module.exports = app
} else {
  // 监听端口，启动程序
  app.listen(config.port, function () {
    console.log(`${pkg.name} listening on port ${config.port}`)
  })
}