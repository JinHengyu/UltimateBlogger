// 下面给出几个常用的搜索 npm 模块的网站：

// http://npmjs.com(npm 官网)
// http://node-modules.com
// https://npms.io
// https://nodejsmodules.org


module.exports = function (app) {
  app.get('/', function (req, res) {
    res.redirect('/posts')
  })
  app.use('/signup', require('./signup'))
  app.use('/signin', require('./signin'))
  app.use('/signout', require('./signout'))
  app.use('/posts', require('./posts'))
  app.use('/comments', require('./comments'))

  app.use('/about', (req, res, next) => {
    res.render('about')
  })


  app.use('/readme', (req, res, next) => {
    res.setHeader('Content-Type', 'text/plain;charset=UTF-8');

    res.send(require('fs').readFileSync(global.all_path.readme))

    // res.sendFile(global.all_path.readme, {}, () => {})
    // the "../" is considered malicious
  })

  // 404 page
  app.use(function (req, res) {
    // 是否发出res
    if (!res.headersSent) {
      res.status(404).render('404')
    }
  })
}